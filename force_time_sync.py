import sys
import numpy
import argparse
import associate


if __name__=="__main__":
    # parse command line
    parser = argparse.ArgumentParser(description='''
    This script forces the timestamp of . 
    ''')
    parser.add_argument('first_file', help='ground truth trajectory (format: timestamp tx ty tz qx qy qz qw). acts as the reference file')
    parser.add_argument('second_file', help='estimated trajectory (format: timestamp tx ty tz qx qy qz qw)')
    parser.add_argument('--frame_rate', help='time offset added to the timestamps of the second file (default: 30.0)',default=30.0)
    args = parser.parse_args()

    print "First file: "+args.first_file+" second file: "+args.second_file+" frame rate: "+args.frame_rate

    initTime = 0.0
    framerate = float(args.frame_rate)
    print 1/framerate

    with open(args.first_file) as f:
    	for line in f:
	    	#content = f.readlines()
	    	if (line[:1] != '#'):
	    		data = line.split(',')
	    		if(len(data)<8):
	    			data = line.split(' ')

	    		initTime = float(data[0])
	    		#print data, len(data), float(data[0])

	    		break


	print args.first_file+' starts off at timestamp: '+str(initTime)
	data_write = []

    with open(args.second_file) as f:
        for line in f:
	        #content = f.readlines()
	        if (line[:1] != '#'):
	    		data = line.split(',')
	    		if(len(data)<8):
	    			data = line.split(' ')

	    		data[0] = str(initTime)
	    		initTime = initTime + (1/framerate)
	    		data_write.extend([data])

	print len(data_write)
	print ', '.join(data_write[0])+'\n'

	with open(args.second_file, "w") as f:
		f.write("")

    with open(args.second_file, "a") as f:
    	for i in range(len(data_write)):
            #print "writing"
            f.write((', '.join(data_write[i]))[:-3]+'\n')