#!/bin/bash
echo "Reading config...." >&2
source Config.cfg

DIRCOUNT=0
roscore_launched=0
data_count=0

echo "$1"

if [ -d $TEMP_DIR ]; then
  	cd $TEMP_DIR;
	timestamp=$(date +%s)
	tar -zcvf "$1_"$timestamp.tar.gz */
	rm -rf */
fi



for item in $DATASET_DIR/*
do 
if [ -d "$item" ]
        then
         DIRCOUNT=$[$DIRCOUNT+1]
fi
done

#Just one level of re-confirmation here
echo "Running LSD SLAM using configuration: $CONFIG_FILE, from dataset directory: $DATASET_DIR containing $DIRCOUNT folders." >&2
read -p "Do you want to continue (Y/n)? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    for item in $DATASET_DIR/*
		do 
		gt_file=""
		config_file_current=""
		if [ -d "$item" ]
		        then
		        dataset=""
		        image_count=0
		        movie_file=""
		         for _item in $item/*
		         do
			         if [ -f "$_item" ]
					    then
					         if [ ${_item: -4} = ".png" ]
					         	then
					         	image_count=$[image_count+1]
					         	if [ $image_count -gt 30 ];
					         		then
					         		dataset=$item
					         	fi
					         elif [[ ${_item: -4} = ".mov" || ${_item: -4} = ".avi" || ${_item: -5} = ".mpeg" ]]; then
					         	#statements
					         	movie_file=$_item
					         elif [[ ${_item: -6} = "GT.csv" || ${_item: -6} = "gt.csv" || ${_item: -15} = "groundtruth.txt" ]]; then

					         	gt_file=$_item
					         fi
				    elif [ -d "$_item" ]
				        then
				         for folders in $_item/*
				         	do
				         		if [ -f "$folders" ]
						    	then
						         if [[ ${folders: -4} = ".png" &&  ${_item##*/} = "rgb" || ${_item##*/} = "image" || ${_item##*/} = "images" ]];
						         	then
						         	image_count=$[image_count+1]
						         	if [ $image_count -gt 30 ];
						         		then
						         		dataset=$_item
						         	fi
						         elif [[ ${folders: -4} = ".mov" || ${folders: -4} = ".avi" || ${folders: -5} = ".mpeg" ]]; then
						         	#statements
						         	movie_file=$folders
						         elif [[ ${folders: -6} = "GT.csv" || ${folders: -6} = "gt.csv" || ${folders: -15} = "groundtruth.txt" ]]; then
						         	gt_file=$folders
						         fi
						     fi
				         	done
					fi
				done

				cleared_for_takeoff=0

				if [[ $dataset == "" && $movie_file == "" ]];
					then
					echo "No dataset or image folder found, skipping directory: $item"
				elif [[ $dataset != "" && $movie_file == "" ]];
					then
					echo "Going to run LSD SLAM on: $item"
					cleared_for_takeoff=1
				elif [[ $dataset == "" && $movie_file != "" ]];
					then
					echo "Found no images. Going to split the video file: $movie_file"
					cd $item
					avconv -i $movie_file -r $FRAME_RATE -f image2 %04d.png
					dataset=$item
					echo "Now running LSD SLAM on $dataset"
					cleared_for_takeoff=1
				elif [[ $dataset != "" && $movie_file != "" ]];
					then
					echo "Found both images and some video files. Going to ignore $movie_file and run LSD SLAM on $item"
					cleared_for_takeoff=1
				fi

				if [[ $cleared_for_takeoff -gt 0 ]]
					then
					echo "LSD LAUNCH"
					if [[ $roscore_launched == 0 ]];
						then
						roscore_launched=1
						tmux new -ds ros roscore
						gnome-terminal -e "bash -c \"cd $PACKAGE_DIR; rosrun lsd_slam_viewer viewer; exec bash\""
					fi

					mkdir -p $TEMP_DIR
					cd $TEMP_DIR;
					mkdir -p $data_count
					cd $data_count
					opFile=$TEMP_DIR'/'$data_count'/Pose_LSD.csv'

					logFile=$TEMP_DIR'/'$data_count'/LSD_SLAM_Log.txt'

					cd $PACKAGE_DIR;
					rosrun lsd_slam_core dataset_slam _files:=$dataset _hz:=$FRAME_RATE _calib:=$CONFIG_FILE  | tee $logFile;

					cd $TEMP_DIR;
					cd $data_count;
					
					mv $OUTPUT_FILE $opFile
					gt_file_target=$TEMP_DIR'/'$data_count'/gt.csv'
					if [[ $gt_file != "" ]]; then
						cp $gt_file $gt_file_target
					else
						cp $opFile $gt_file_target
					fi

					imgFile=$TEMP_DIR'/'$data_count'/ate.png'
					ate_text=$TEMP_DIR'/'$data_count'/ate.txt'
					ate_python_file=$WORKSPACE_DIR'/'evaluate_ate.py

					if [[ $FORCE_SYNC = "true" ]]; 
						then
						force_sync_file=$WORKSPACE_DIR'/'force_time_sync.py
						python $force_sync_file $gt_file_target $opFile --frame_rate $FRAME_RATE
						echo "python $force_sync_file $gt_file_target $opFile --frame_rate $FRAME_RATE "
					fi

					python $ate_python_file $gt_file_target $opFile --plot $imgFile --offset 0 --scale 1 --verbose > $ate_text

					rpeImgFile=$TEMP_DIR'/'$data_count'/rpe.png'
					rpe_text=$TEMP_DIR'/'$data_count'/rpe.txt'
					rpe_python_file=$WORKSPACE_DIR'/'evaluate_rpe.py

					python $rpe_python_file $gt_file_target $opFile --fixed_delta --plot $rpeImgFile --offset 0 --scale 1 --verbose > $rpe_text
				fi

				data_count=$[$data_count+1]

		fi
		done

		tmux kill-session -t ros 
		
fi